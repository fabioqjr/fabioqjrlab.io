---
layout: default
title: Home
nav_order: 1
description: "Boas vindas à Wiki do time de front da Vnda!"
permalink: /
---

# Boas vindas à Wiki do time de front da Vnda!
{: .fs-9 }
Aqui você encontra referências e guias que ajudam você a criar novas lojas e entender o funcionamento das já existentes, permitindo que você use o máximo da plataforma da Vnda e do Liquid — e mantendo um código claro e organizado no caminho.
{: .fs-6 .fw-300 }

[Vamos começar!](#onde-começar){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 }

---

## Onde começar

### Setups

A documentação ideal para quem está começando. Entenda como funciona o processo de criação de uma nova loja da Vnda, a estrutura de um projeto e como integrar sua loja com serviços externos, como o Instagram e o Tumblr.

- [Introdução ao setup]({{ site.baseurl }}{% link docs/introducao.md %})
- [Começando um novo setup]({{ site.baseurl }}{% link docs/introducao.md %})
- [A estrutura de um setup]({{ site.baseurl }}{% link docs/introducao.md %})
- Integrações com serviços externos *— em breve!*

### Liquid

Ideal para tirar dúvidas e ampliar seus conhecimentos no Liquid, a linguagem de marcação que usamos para dinamizar o layout das lojas Vnda.

- [Introdução ao Liquid]({{ site.baseurl }}{% link docs/introducao.md %})
- [Referência de Objetos]({{ site.baseurl }}{% link docs/introducao.md %})
- [Referência de Tags]({{ site.baseurl }}{% link docs/introducao.md %})
- [Referência de Filtros]({{ site.baseurl }}{% link docs/introducao.md %})

### Liquid

Tutoriais da equipe para casos específicos ou soluções criativas para problemas frequentes. Fez algo bacana? [Crie um guia](https://gitlab.com/vnda/front/wikis/guias/Meu%20novo%20guia)!

